            //click button
function showandhide() {
    if($(".hidden").is(":hidden")) {
        $('.showmore').html("Скрыть <");
    } else {
        $('.showmore').html("Смотреть ещё >");
    }
    $(".hidden").slideToggle();
//    $('.slick-in').slick('slickNext')
};
function goNewSlick(urlka){
    $('.giphy').fadeIn(300, function(){
//        $('.slick-in').slick('unslick');
        $('#podzamenu').empty();
        $.ajax({
            url: "/files/" + urlka + ".txt",
            success: function( data ) {
            $('#podzamenu').append(data);
            setTimeout(1000);
        }});
    });
    setTimeout( function(){
        $('.slick-in').slick({
            arrows: true,
            variableWidth: true,
            autoplay: true
        });
    }, 1000);
    setTimeout( function(){
        $('.giphy').fadeOut(200);
    }, 1000);
    setTimeout( function(){
        $('.slick-in').slick({
            arrows: true,
            variableWidth: true,
            autoplay: true
        });
    }, 3000);
};
            //123
$(".block-categories__all li a").click(function(e){
    if ($(e.target).hasClass("block-categories__active")) {
        return false;
    }
    e.preventDefault();
    $(".block-categories__active").removeClass();
    $(this).addClass("block-categories__active");
    var urlka = $(this).data('url');
    goNewSlick(urlka);
    setTimeout(function(){
        $('.slick-in img').magnificPopup({type:'image'});
    },1000);
});


                //mobile

$(".block-categories__mobile-selecter select").change(function(){
    var urlka = $(".block-categories__mobile-selecter select").val();
    goNewSlick(urlka);
    setTimeout(function(){
        $('.slick-in img').magnificPopup({type:'image'});
    },1000);
//    setTimeout(function(){
//        $('.slick-in').slick({
//            arrows: true,
//            variableWidth: true,
//            autoplay: true
//        });
//    }, 1000);
});

$(".header__mobile").click(function(){
    $(".header__menu").slideToggle();
    $(".header__in-span_first").toggleClass("header__in-span_first_click");
    $(".header__in-span_second").toggleClass("header__in-span_second_click");
    $(".header__main-span_first").toggleClass("header__main-span_first_click");
    $(".header__main-span_second").toggleClass("header__main-span_second_click");
    $(".header__mobile").toggleClass("header__main-span_click");
});


                //bounce

$("#bounce").click(function(){
    $("#block-bounce_h").fadeIn(500);
});
$("#close").click(function(){
    $("#block-bounce_h").fadeOut(500);
});
$(document).mousedown(function (e){
    var div = $(".blackback");
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        $('#block-bounce_h').fadeOut(500);
    }
});

