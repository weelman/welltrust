<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="Разработчик Litvinenko Digital">
    <meta name="description" content="правила сайта, политика конфиденциальности">
    <meta name=«title» content="Политика конфиденциальности">
    <link rel="canonical" href="http://arenda-pomeshcheniy38.ru/">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:url" content="http://arenda-pomeshcheniy38.ru/">
    <title>Политика конфиденциальности</title>
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/png" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/faq.css">
    <!--  fonts   -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900" rel="stylesheet"></head>
    <!--  map   -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 17,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(52.283202, 104.277974), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [ { "featureType": "all", "elementType": "all", "stylers": [ { "saturation": -100 }, { "gamma": 0.5 } ] } ]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(52.283202, 104.277974),
                    map: map,
                    icon: {
                        url: "../../img/icon/map-marker.png",
                        scaledSize: new google.maps.Size(32, 64)
                    }
                });
            }
        </script>
<body>
    <div class="wrapper">
        <header class="header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="header__logo col-lg-auto">
                        <div class="row align-items-end m-0"> <span><img src="../../img/main/header__logo.png" alt=""></span> <span class="company">ООО «ВЭЛТРАСТ»</span> </div> <address>г.Иркутск ул. Степана Разина 27</address> </div>
                    <div class="header__menu col">
                        <ul class="nav">
                            <li class="nav-item"> <a class="nav-link" href="/"> < вернуться</a> </li>
                        </ul>
                    </div>
                    <div class="header__phone col-lg-auto"> <img src="../../img/icon/phone.jpg" alt=""> <a href="tel:83952334050">8 (3952) 33-40-50</a> </div>
                </div>
            </div>
        </header>
        <section id="privacy">
            <div class="container">
            <h1 style="font-size: 26px">ПОЛИТИКА В ОТНОШЕНИИ ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ В ООО «Вэлтраст»</h1>
            <p>
                <br>1. Общие положения
                <br>1.1. Политика в отношении обработки персональных данных (далее — Политика)направлена на защиту прав и свобод физических лиц, персональные данные которых обрабатывает ООО «Вэлтраст» (далее — Оператор).
                <br>1.2. Политика разработана в соответствии с п. 2 ч. 1 ст. 18.1 Федерального закона от 27 июля2006 г. № 152-ФЗ «О персональных данных» (далее — ФЗ «О персональных данных»).
                <br>1.3. Политика содержит сведения, подлежащие раскрытию в соответствии с ч. 1 ст. 14 ФЗ «О персональных данных», и является общедоступным документом.
                <br>
                <br>2. Сведения об операторе
                <br>2.1. Оператор ведет свою деятельность по адресу 664025, г. Иркутск, ул.Степана Разина 27.
                <br>2.2. Директор: Дмитрий Михайлович (телефон +7 914 927 91 97 назначен ответственным за организацию обработки персональных данных.
                <br>2.3. База данных информации, содержащей персональные данные граждан Российской Федерации, находится по адресу: trast38@mail.ru
                <br>
                <br>3. Сведения об обработке персональных данных
                <br>3.1. Оператор обрабатывает персональные данные на законной и справедливой основе для выполнения возложенных законодательством функций, полномочий и обязанностей, осуществления прав и законных интересов Оператора, работников Оператора и третьих лиц.
                <br>3.2. Оператор получает персональные данные непосредственно у субъектов персональных данных.
                <br>3.3. Оператор обрабатывает персональные данные автоматизированным и неавтоматизированным способами, с использованием средств вычислительной техники и без использования таких средств.
                <br>3.4. Действия по обработке персональных данных включают сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление и уничтожение.
                <br>3.5. Базы данных информации, содержащей персональные данные граждан Российской Федерации, находятся на территории Российской Федерации.
                <br>
                <br>4. Обработка персональных данных клиентов
                <br>4.1. Оператор обрабатывает персональные данные клиентов в рамках правоотношений с Оператором, урегулированных частью второй Гражданского Кодекса Российской Федерации от 26 января 1996 г. № 14-ФЗ, (далее — клиентов).
                <br>4.2. Оператор обрабатывает персональные данные клиентов в целях соблюдения норм законодательства РФ, а также с целью:
                <br>— заключать и выполнять обязательства по договорам с клиентами;
                <br>— осуществлять виды деятельности, предусмотренные учредительными документами ООО «Вэлтраст»
                <br>— информировать о новых продуктах, специальных акциях и предложениях;
                <br>— информировать о новых статьях, видео и мероприятиях;
                <br>— выявлять потребность в продуктах;
                <br>— определять уровень удовлетворённости работы.
                <br>4.3. Оператор обрабатывает персональные данные клиентов с их согласия, предоставляемого на срок действия заключенных с ними договоров. В случаях, предусмотренных ФЗ «О персональных данных», согласие предоставляется в письменном виде. В иных случаях согласие считается полученным при заключении договора или при совершении конклюдентных действий.
                <br>4.4. Оператор обрабатывает персональные данные клиентов в течение сроков действия заключенных с ними договоров. Оператор может обрабатывать персональные данные клиентов после окончания сроков действия заключенных с ними договоров в течение срока, установленного п. 5 ч. 3 ст. 24 части первой НК РФ, ч. 1 ст. 29 ФЗ «О бухгалтерском учёте» и иными нормативными правовыми актами.
                <br>4.5. Оператор обрабатывает следующие персональные данные клиентов:
                <br>— Фамилия, имя, отчество;
                <br>— Тип, серия и номер документа, удостоверяющего личность;
                <br>— Дата выдачи документа, удостоверяющего личность, и информация о выдавшем его органе;
                <br>— Год рождения;
                <br>— Месяц рождения;
                <br>— Дата рождения;
                <br>— Место рождения;
                <br>— Адрес;
                <br>— Номер контактного телефона;
                <br>— Адрес электронной почты;
                <br>— Идентификационный номер налогоплательщика;
                <br>— Номер страхового свидетельства государственного пенсионного страхования;
                <br>— Должность;
                <br>— Фотография.
                <br>4.6. Для достижения целей обработки персональных данных и с согласия клиентов Оператор предоставляет персональные данные или поручает их обработку следующим лицам:
                <br>— менеджер по продажам
                <br>— руководитель проекта
                <br>— менеджер проекта
                <br>— маркетолог
                <br>
                <br>5. Сведения об обеспечении безопасности персональных данных
                <br>5.1. Оператор назначает ответственного за организацию обработки персональных данных для выполнения обязанностей, предусмотренных ФЗ «О персональных данных» и принятыми в соответствии с ним нормативными правовыми актами.
                <br>5.2. Оператор применяет комплекс правовых, организационных и технических мер по обеспечению безопасности персональных данных для обеспечения конфиденциальности персональных данных и их защиты от неправомерных действий:
                <br>— обеспечивает неограниченный доступ к Политике, копия которой размещена по адресу нахождения Оператора, а также может быть размещена на сайте Оператора (при его наличии);
                <br>— во исполнение Политики утверждает и приводит в действие документ «Положение об обработке персональных данных» (далее — Положение) и иные локальные акты;
                <br>— производит ознакомление работников с положениями законодательства о персональных данных, а также с Политикой и Положением;
                <br>— осуществляет допуск работников к персональным данным, обрабатываемым в информационной системе Оператора, а также к их материальным носителям только для выполнения трудовых обязанностей;
                <br>— устанавливает правила доступа к персональным данным, обрабатываемым в информационной системе Оператора, а также обеспечивает регистрацию и учёт всех действий с ними;
                <br>— производит оценку вреда, который может быть причинен субъектам персональных данных в случае нарушения ФЗ «О персональных данных»;
                <br>— производит определение угроз безопасности персональных данных при их обработке в информационной системе Оператора;
                <br>— применяет организационные и технические меры и использует средства защиты информации, необходимые для достижения установленного уровня защищенности персональных данных;
                <br>— осуществляет обнаружение фактов несанкционированного доступа к персональным данным и принимает меры по реагированию, включая восстановление персональных данных, модифицированных или уничтоженных вследствие несанкционированного доступа к ним;
                <br>— производит оценку эффективности принимаемых мер по обеспечению безопасности персональных данных до ввода в эксплуатацию информационной системы Оператора;
                <br>— осуществляет внутренний контроль соответствия обработки персональных данных ФЗ «О персональных данных», принятым в соответствии с ним нормативным правовым актам, требованиям к защите персональных данных, Политике, Положению и иным локальным актам, включающий контроль за принимаемыми мерами по обеспечению безопасности персональных данных и их уровня защищенности при обработке в информационной системе Оператора.
                <br>
                <br>6. Права субъектов персональных данных
                <br>6.1. Субъект персональных данных имеет право:
                <br>— на получение персональных данных, относящихся к данному субъекту, и информации, касающейся их обработки;
                <br>— на уточнение, блокирование или уничтожение его персональных данных в случае, если они являются неполными, устаревшими, неточными, незаконно полученными или не являются необходимыми для заявленной цели обработки;
                <br>— на отзыв данного им согласия на обработку персональных данных;
                <br>— на защиту своих прав и законных интересов, в том числе на возмещение убытков и компенсацию морального вреда в судебном порядке;
                <br>— на обжалование действий или бездействия Оператора в уполномоченный орган по защите прав субъектов персональных данных или в судебном порядке.
                <br>6.2. Для реализации своих прав и законных интересов субъекты персональных данных имеют право обратиться к Оператору либо направить запрос лично или с помощью представителя. Запрос должен содержать сведения, указанные в ч. 3 ст. 14 ФЗ «О персональных данных».
                <br>
                <br>УТВЕРЖДАЮ
                <br>Генеральный директор ООО «Вэлтсраст» Дмитрий Михайлович
                <br>
                <br>
                <br>02.03.2018</p>
            <br>
            <br>
            <br> </div>
        </section>
        <section id="block-map">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <span>МЫ НА КАРТЕ<br>
                        <small>г.Иркутск, улица Степана Разина, дом 27, офис 11</small></span>
                    </div>
                </div>
            </div>
            <div id="map"></div>
        </section>
    </div>
    <footer class="footer">
        <div class="footer__info">
            <div class="container">
                <div class="row footer__info_first align-items-end">
                    <div class="col-lg-2"> <img class="footer__info-img" src="../../img/main/footer__logo.png" alt=""> </div>
                    <div class="col">
                        <p>ОГРН:1123850047133
                            <br> ИНН/КПП: 380226903/38081001
                            <br> ОКПО: 41781216
                            <br> Юридический адрес: 664025, Иркутская область,
                            <br> г.Иркутск, улица Степана Разина, дом 27, офис 11</p>
                    </div>
                    <div class="col-lg-auto footer__info_first_socnet">
                        <p>Мы в социальных сетях</p>
                        <section>
                            <a href="https://www.instagram.com/litvinenko.digital/" target="_blank">
                                <figure class="instagram"></figure>
                            </a>
                            <a href="https://vk.com/arenda_pomeshcheniy38" target="_blank">
                                <figure class="vk"></figure>
                            </a>
                            <a href="https://www.facebook.com/%D0%9E%D0%9E%D0%9E-%D0%92%D1%8D%D0%BB%D1%82%D1%80%D0%B0%D1%81%D1%82-720149325039318/" target="_blank">
                                <figure class="fb"></figure>
                            </a>
                        </section>
                    </div>
                </div>
                <div class="row footer__info_middle">
                    <div class="col-lg-4 offset-lg-2">
                        <p> тел/факс: <a href="tel:83952334050">(3952) 33-40-50</a>, <a href="tel:83952333378">33-33-78</a>
                            <br> E-mail: <a href="mailto:trast38@mail.ru">trast38@mail.ru</a> </p>
                    </div>
                </div>
                <div class="row footer__info_last justify-content-end">
                    <div class="col-lg-auto"> <a href="/privacy">Политика конфиденциальности</a> </div>
                </div>
            </div>
        </div>
        <div class="footer__developer"> <a href="https://litvinenko.digital/" target="_blank">разработка сайта - LITVINENKO</a> </div>
    </footer>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
</body>

</html>
