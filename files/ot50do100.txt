<div class="col-lg-4 col-6 item">
    <div class="slick-in"><img src="/img/office/50for100m2/1/1.jpg" href="/img/office/50for100m2/1/1.jpg" alt=""><img src="/img/office/50for100m2/1/2.jpg" href="/img/office/50for100m2/1/2.jpg" alt=""><img src="/img/office/50for100m2/1/3.jpg" href="/img/office/50for100m2/1/3.jpg" alt=""></div>
    <p>10 этаж
        <br>
        <br>74 м2 / 4 помещения
        <br>+ коммунальные включены
        <br>+ ежедневная уборка
        <br>+ бесплатная парковка
        <br>+ охрана</p>
</div>
<div class="col-lg-4 col-6 item">
    <img src="/img/office/50for100m2/2/1.jpg" href="/img/office/50for100m2/2/1.jpg" alt="">
    <p>1 этаж
        <br>
        <br>71,2 м2
        <br>+ коммунальные включены
        <br>+ ежедневная уборка
        <br>+ бесплатная парковка
        <br>+ охрана</p>
</div>
<div class="col-lg-4 col-6 item">
    <div class="slick-in">
    <img src="/img/office/50for100m2/3/1.jpg" href="/img/office/50for100m2/3/1.jpg" alt="">
    <img src="/img/office/50for100m2/3/2.jpg" href="/img/office/50for100m2/3/2.jpg" alt="">
    <img src="/img/office/50for100m2/3/3.jpg" href="/img/office/50for100m2/3/3.jpg" alt=""></div>
    <p>8 этаж
        <br>
        <br>69,5 м2
        <br>+ коммунальные включены
        <br>+ ежедневная уборка
        <br>+ бесплатная парковка
        <br>+ охрана</p>
</div>
<div class="hidden container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-4 col-6 item">
            <div class="slick-in">
            <img src="/img/office/50for100m2/4/1.jpg" href="/img/office/50for100m2/4/1.jpg" alt="">
            <img src="/img/office/50for100m2/4/2.jpg" href="/img/office/50for100m2/4/2.jpg" alt=""></div>
            <p>1 этаж
                <br>
                <br>71,2 м2
                <br>+ коммунальные включены
                <br>+ ежедневная уборка
                <br>+ бесплатная парковка
                <br>+ охрана</p>
        </div>
    </div>
</div>
<div class="forbutton">
    <button class="showmore" onclick="showandhide()">Смотреть еще ></button>
</div>