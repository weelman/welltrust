<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="Разработчик Litvinenko Digital">
    <meta name="description" content="Сдаются нежилые офисные помещения в центре города. Аренда помещений Иркутск. Аренда офиса за 1 день после просмотра. Звоните!">
    <meta name=«title» content="Аренда помещений Иркутск">
    <link rel="canonical" href="http://arenda-pomeshcheniy38.ru/">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:url" content="http://arenda-pomeshcheniy38.ru/">
    <title>Аренда помещений Иркутск</title>
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/magnif.css">
    <link rel="stylesheet" href="/css/animate.min.css">
    <script src="/js/wow.min.js">
    </script>
    <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter47979902 = new Ya.Metrika2({
                            id:47979902,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true,
                            trackHash:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/47979902" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    <script>
        new WOW().init();
    </script>
    <!--  fonts   -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900" rel="stylesheet"> </head>
<!--  map   -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
<script type="text/javascript">
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 17, // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(52.283202, 104.277974), // New York
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{
                "featureType": "all"
                , "elementType": "all"
                , "stylers": [{
                    "saturation": -100
                }, {
                    "gamma": 0.5
                }]
            }]
        };
        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');
        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);
        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(52.283202, 104.277974)
            , map: map
            , icon: {
                url: "img/icon/map-marker.png"
                , scaledSize: new google.maps.Size(32, 64)
            }
        });
    }
</script>

<body>
   <div id="block-bounce_h">
       <div class="blackback">
           <div class="block-bounce_h__in">
               <span id="close"></span>
               <p>ЗДРАВСТВУЙТЕ!
               <br><small>звоните нам по прямому номеру</small></p>
               <a href="tel:+73952334050"><span class="phone"></span>+7 3952 33-40-50</a>
           </div>
       </div>
   </div>
    <div id="bounce"></div>
    <div class="wrapper">
        <header class="header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="header__logo col-lg-auto col-12">
                        <div class="header__mobile d-block d-md-none">
                            <span class="header__main-span header__main-span_first">
                                <span class="header__in-span header__in-span_first"></span>
                            </span>
                            <span class="header__main-span header__main-span_second">
                                <span class="header__in-span header__in-span_second"></span>
                            </span>
                        </div>
                        <div class="row align-items-end justify-content-center m-0"> <span><img src="img/main/header__logo.png" alt=""></span> <span class="company">ООО «ВЭЛТРАСТ»</span> </div> <address class="d-none d-md-block text-center">г.Иркутск ул. Степана Разина 27</address> </div>
                    <div class="header__menu col">
                        <ul class="nav">
                            <li class="nav-item"> <a class="nav-link" href="#block-asking">Преимущества</a> </li>
                            <li class="nav-item d-md-block d-none"> <a class="nav-link" href="#block-faq">Условия аренды</a> </li>
                            <li class="nav-item nav-item_last"> <a class="nav-link" href="#block-map">Посмотреть на карте</a> </li>
                            <li class="nav-item nav-item-mob d-md-none d-block"><a class="nav-link" href="tel:83952334050">8 (3952) 33-40-50</a></li>
                        </ul>
                    </div>
                    <div class="header__phone col-lg-auto d-md-block d-none text-center"> <img src="img/icon/phone.jpg" alt=""> <a href="tel:83952334050">8 (3952) 33-40-50</a> </div>
                </div>
            </div>
        </header>
        <section class="block-offer">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-lg-8 offset-lg-4 text-right block-offer__first">
                        <h1>Аренда помещений Иркутск</h1> <span>Нежилые офисные помещения в аренду от собственника</span>
                        <p>офисы от 10 м2 до 500 м2</p>
                        <a href="#block-categories">
                            <button>Выбрать офисное помещение ></button>
                        </a>
                    </div>
                    <div class="col-lg-auto offset-lg-4 block-offer__download d-lg-block d-none">
                        <a href="files/com.pdf" download>
                            <div class="row align-items-center">
                                <div class="col"> <img src="img/main/pdf.png" alt=""> </div>
                                <div class="col p-0">СКАЧАТЬ
                                    <br> КОММЕРЧЕСКОЕ
                                    <br> ПРЕДЛОЖЕНИЕ </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <div id="block-categories">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12"> <span>ВЫБЕРИТЕ КАТЕГОРИЮ</span> </div>
                    <div class="block-categories__all w-100">
                       <div class="block-categories__mobile-selecter">
                           <select>
                               <option value="do20">до 20м2</option>
                               <option value="ot20do30">от 20 до 30м2</option>
                               <option value="ot30do50">от 30 до 50м2</option>
                               <option value="ot50do100">от 50 до 100м2</option>
                               <option disabled>Крупные площадь</option>
                               <option value="ot100do200">от 100 до 200 м2</option>
                               <option value="ot200do300">от 200 до 300 м2</option>
                               <option value="500">500 м2</option>
                           </select>
                       </div>
                        <div class="col-md-3 d-none d-md-block">
                            <p>Средняя площадь</p>
                            <ul>
                                <li><a href="" data-url="do20" class="block-categories__active">до 20м2</a></li>
                                <li><a href="" data-url="ot20do30">от 20 до 30м2</a></li>
                                <li><a href="" data-url="ot30do50">от 30 до 50м2</a></li>
                                <li><a href="" data-url="ot50do100">от 50 до 100м2</a></li>
                            </ul>
                            <p>Крупные площадь</p>
                            <ul>
                                <li><a href="" data-url="ot100do200">от 100 до 200 м2</a></li>
                                <li><a href="" data-url="ot200do300">от 200 до 300 м2</a></li>
                                <li><a href="" data-url="500">500 м2</a></li>
                            </ul>
                        </div>

                        <div class="col-md-9 col-12 text-center">
                            <div class="giphy"><img src="css/ajax-loader.gif" alt=""></div>
                            <div id="podzamenu" class="row m-0 text-left justify-content-center">
                                <div class="col-lg-4 col-6 item">
                                    <div class="slick-in">
                                        <img src="img/office/20m2/1/1.jpg" href="img/office/20m2/1/1.jpg" alt="">
                                        <img src="img/office/20m2/1/2.jpg" href="img/office/20m2/1/2.jpg" alt="">
                                        <img src="img/office/20m2/1/3.jpg" href="img/office/20m2/1/3.jpg" alt="">
                                    </div>
                                    <p>10 этаж
                                        <br>
                                        <br> 14.5 м2
                                        <br> + коммунальные включены
                                        <br> + ежедневная уборка
                                        <br> + бесплатная парковка
                                        <br> + охрана</p>
                                </div>
                                <div class="col-lg-4 col-6 item">
                                    <div class="slick-in"> <img src="img/office/20m2/2/1.jpg" href="img/office/20m2/2/1.jpg" alt=""> <img src="img/office/20m2/2/2.jpg" href="img/office/20m2/2/2.jpg" alt=""> <img src="img/office/20m2/2/3.jpg" href="img/office/20m2/2/3.jpg" alt=""> </div>
                                    <p>10 этаж
                                        <br>
                                        <br> 12.1 м2
                                        <br> + коммунальные включены
                                        <br> + ежедневная уборка
                                        <br> + бесплатная парковка
                                        <br> + охрана</p>
                                </div>
                                <div class="col-lg-4 col-6 item">
                                    <div class="slick-in"> <img src="img/office/20m2/3/1.jpg" href="img/office/20m2/3/1.jpg" alt=""> <img src="img/office/20m2/3/2.jpg" href="img/office/20m2/3/2.jpg" alt=""> </div>
                                    <p>4 этаж
                                        <br>
                                        <br> 18,1 м2
                                        <br> + коммунальные включены
                                        <br> + ежедневная уборка
                                        <br> + бесплатная парковка
                                        <br> + охрана</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="seotext">
            <div class="container">
                <p>Мы предлагаем аренду - офисные помещения разных площадей в г. Иркутске. В здании сдаются офисные помещения на втором, четвертом, пятом и восьмом этажах. Аренда помещений Иркутск. Если вы планируете снять коммерческую недвижимость в Иркутске под офис, то вам однозначно стоит обратить внимание на наше здание. Собственник сдает. Здание расположено на пересечении улиц Степана Разина и Свердлова. Рядом остановки, бизнес центры, большая парковка и круглосуточная охрана. Коммунальные услуги уже включены в арендную плату офиса, при этом ежедневно в офисном здании проводится уборка всех помещений. Коммерческую недвижимость в самом центре Иркутска найти достаточно непросто, при этом мы готовы обеспечить вас необходимой мебелью: столы, стулья, тумбочки и шкафы, вы можете получить в аренду бесплатно на весь срок аренды по договору. Сдается офис для небольшой организации от 10 кв. м. до 100 кв. м. - 500 кв. м. Стоимость аренды уточняйте у администратора. Здание расположено в центре делового Иркутска с удобной парковкой. Мы предлагаем долгосрочную аренду производственных помещений свободного назначения с хорошим ремонтом на длительный срок.</p>
            </div>
        </div>
        <section id="block-asking">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <h2>Планируете снять офис,<br>
                        но у вас нет мебели?</h2>
                        <p>мы предоставим
                            <br> <span>мебель бесплатно!</span></p> *на весь период договора аренды,
                        <br> звоните! </div>
                    <div class="col-lg-8 wow fadeInRight"> <img class="w-100" src="img/main/block-asking__office.jpg" alt=""> </div>
                </div>
            </div>
        </section>
        <section class="block-advantage">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>НАШИ ПРЕИМУЩЕСТВА</h2> </div>
                    <div class="col-md-2 col-6 text-center wow fadeInUp" data-wow-delay="0.3s"><img src="img/main/block-advantage_item%5B1%5D.jpg" alt="удобная развязка, центр города"> <span>удобная развязка <br>
                    центр города</span></div>
                    <div class="col-md-2 col-6 text-center wow fadeInUp" data-wow-delay="0.8s"><img src="img/main/block-advantage_item%5B2%5D.jpg" alt="доступ в офис 24 часа"> <span>доступ в офис<br> 24 часа</span></div>
                    <div class="col-md-2 col-6 text-center wow fadeInUp" data-wow-delay="1.3s"><img src="img/main/block-advantage_item%5B3%5D.jpg" alt=""><span>ежедневная уборка<br> в каждом офисе</span></div>
                    <div class="col-md-2 col-6 text-center wow fadeInUp" data-wow-delay="1.7s"><img src="img/main/block-advantage_item%5B4%5D.jpg" alt=""><span>охраняемое здание<br>24 часа в сутки</span></div>
                    <div class="col-md-2 col-6 text-center wow fadeInUp" data-wow-delay="2s"><img src="img/main/block-advantage_item%5B5%5D.jpg" alt="Бесплатная парковка Иркутск"><span>бесплатная<br>парковка</span></div>
                    <div class="col-md-2 col-6 text-center wow fadeInUp" data-wow-delay="2.2s"><img src="img/main/block-advantage_item%5B6%5D.jpg" alt="вкусная столовая Иркутск"><span>вкусная столовая<br>с домашней едой</span></div>
                </div>
            </div>
        </section>
        <section class="block-architect">
            <div class="container">
                <div class="row block-architect_img">
                    <div class="col-12">
                        <h2>Архитектура</h2> </div>
                    <div class="col-6"> <img src="img/main/block-architect__item%5B1%5D.jpg" alt="Иркутск офис"> </div>
                    <div class="col-6"> <img src="img/main/block-architect__item%5B2%5D.jpg" alt="Иркутск офис"> </div>
                </div>
                <p>Офисное здание по адресу ул. Степана Разина 27 было сдано в эксплуатацию в 1984 году. Спроектировано компанией «ИРКУТСКГРАЖДАНПРОЕКТ». Монолитно-каркасное здание с кирпичным заполнением. Просторный холл, удобное место расположения. Облицовка здания не перенасыщена синтетическими материалами как в большинстве бизнес центров, благодаря этому «здание дышит», что положительно влияет на рабочую атмосферу вашего персонала. Четыре интернет провайдера на ваш выбор, с возможностью подключения за один-два дня. Чтобы арендовать офис в нашем здании обратитесь по телефону на сайте, также вы можете приехать к нам на экскурсию лично и посмотреть все свободные помещения. Офисы с большими окнами, высокими потолками до 3-х метров. В нашем здании уже находятся более 30 различных фирм, которые охватывают разные виды деятельности, от небольших компаний до федеральных представителей и банков.</p>
            </div>
        </section>
        <section class="block-providers">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h2>интернет провайдеры в здании</h2> </div>
                    <div class="col-md-2 col-6"><img src="img/main/block-providers__item%5B1%5D.jpg" alt=""></div>
                    <div class="col-md-2 col-6"><img src="img/main/block-providers__item%5B2%5D.jpg" alt=""></div>
                    <div class="col-md-2 col-6"><img src="img/main/block-providers__item%5B3%5D.jpg" alt=""></div>
                    <div class="col-md-2 col-6"><img src="img/main/block-providers__item%5B4%5D.jpg" alt=""></div>
                    <div class="col-md-3 wow fadeInRight">
                        <h4>возможность подключения<br>
                        интернета и телефонии<br>
                        в течение 2-ух дней<br>
                        с момента заявки</h4></div>
                </div>
            </div>
        </section>
        <div class="block-stages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-8">ВСЕ ЭТАПЫ ЗАЕЗДА В 1 ДЕНЬ</div>
                    <div class="col-md-3 col-6"><img class="wow zoomIn" src="img/icon/telephone.png" data-wow-delay="0.5s" alt="">
                        <p>ЗВОНОК</p> <span class="d-none d-lg-inline">договариваемся на осмотр<br>
                    офисного помещения</span>
                        <span class="info">8 (3952) 33-40-50</span></div>
                    <div class="col-md-3 col-6"><img class="wow zoomIn" src="img/icon/placeholder.png" data-wow-delay="1s" alt="">
                        <p>ОСМОТР ПОМЕЩЕНИЯ</p> <span class="d-none d-lg-inline">выбираем помещение<br>
                    которое вам подойдет</span>
                        <span class="info">Степана Разина 27</span></div>
                    <div class="col-md-3 col-6"><img class="wow zoomIn" src="img/icon/contract.png" data-wow-delay="1.5s" alt="">
                        <p>ОФОРМЛЕНИЕ</p> <span class="d-none d-lg-inline">заключаем договор на удобное<br>
                    время от 1месяца до 5 лет</span>
                        <span class="info">Договор аренды</span></div>
                    <div class="col-md-3 col-6"><img class="wow zoomIn" src="img/icon/like.png" data-wow-delay="2s" alt="">
                        <p>ЗАЕЗЖАЕМ В ОФИС</p> <span class="d-none d-lg-inline">заезжаете в офис<br>
                    и можете работать</span>
                        <span class="info">Счастливая работа</span></div>
                </div>
            </div>
        </div>
        <div class="block-inbuild">
            <div class="container">
                <div class="row">
                    <div class="col-12"> В НАШЕМ ЗДАНИИ УЖЕ НАХОДЯТСЯ </div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__sberbank.jpg" alt="сбербанк"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__aeroflot.jpg" alt="аэрофлот"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__litvinenko.jpg" alt="литвиненко"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__grand.jpg" alt="гранд"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__igp.jpg" alt="игп"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__hermes.jpg" alt="гермес"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__energogarant.jpg" alt="энергогарант"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__labirintru.jpg" alt="лабиринтру"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__attika.jpg" alt="аттика"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__pickpoint.jpg" alt="пикпоинт"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__kontinent.jpg" alt="континент"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__zotochka.jpg" alt="заточка"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__artgroup.jpg" alt="артгрупп"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__nightlite.jpg" alt="найтлайт"></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__businesmecanic.jpg" alt=""></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__nautilus.jpg" alt=""></div>
                    <div class="col-md-2 col-4"><img src="img/logo/logo__m.jpg" alt=""></div>
                    <div class="col-md-2 col-4 wow zoomInUp" data-wow-delay="0s"><img src="img/logo/doyouwannabethere.jpg" alt=""></div>
                </div>
            </div>
        </div>
        <div class="block-comercial">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 offset-lg-1">
                        <div class="row align-items-center">
                            <div class="col-lg-auto"> <img src="img/main/pdf-black.png" alt=""> </div>
                            <div class="col"> <span>Скачайте актуальное<br>
                                коммерческое предложение</span>
                                <p>цены, планировки, условия</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 wow flash" data-wow-delay="1s">
                        <a href="files/com.pdf" download>
                            <button>СКАЧАТЬ</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="block-faq">
            <div class="container">
                <div class="row">
                    <div class="col-12"> <span>РАЗДЕЛ FAQ<br>
                        <small>ответы на популярные вопросы</small></span> </div>
                    <div class="col-lg-6"><a href="/faq/arendnaya-plata/" target="_blank">Изменится ли арендная плата во время действия договора?</a></div>
                    <div class="col-lg-6"><a href="/faq/yuridicheskij-adres/" target="_blank">Могу ли я получить юридический адрес на организацию?</a></div>
                    <div class="col-lg-6"><a href="/faq/dokumenti/" target="_blank">Какие документы необходимы для договора аренды?</a></div>
                    <div class="col-lg-6"><a href="/faq/arenda-ofisa/" target="_blank">Условия аренды помещения под офис</a></div>
                    <div class="col-lg-6"><a href="/faq/parkovka-dlya-avtomobilej/" target="_blank">Есть ли парковка? Платная или бесплатная?</a></div>
                </div>
            </div>
        </div>
        <section id="block-map">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12"> <span>МЫ НА КАРТЕ<br>
                        <small>г.Иркутск, улица Степана Разина, дом 27, офис 11</small></span> </div>
                </div>
            </div>
            <div id="map"></div>
        </section>
    </div>
    <footer class="footer">
        <div class="footer__info">
            <div class="container">
                <div class="row footer__info_first align-items-end">
                    <div class="col-lg-2 col-auto"> <img class="footer__info-img" src="img/main/footer__logo.png" alt=""> </div>
                    <div class="col-auto footer__info_first_socnet d-block d-lg-none">
                        <p>Мы в социальных сетях</p>
                        <section>
                            <a href="https://www.instagram.com/arenda.irkutsk.office/" rel=”nofollow” target="_blank">
                                <figure class="instagram"></figure>
                            </a>
                            <a href="https://vk.com/arenda_pomeshcheniy38" rel=”nofollow” target="_blank">
                                <figure class="vk"></figure>
                            </a>
                            <a href="https://www.facebook.com/%D0%9E%D0%9E%D0%9E-%D0%92%D1%8D%D0%BB%D1%82%D1%80%D0%B0%D1%81%D1%82-720149325039318/" rel=”nofollow” target="_blank">
                                <figure class="fb"></figure>
                            </a>
                        </section>
                    </div>
                    <div class="col">
                        <p>ОГРН:1123850047133
                            <br> ИНН/КПП: 380226903/38081001
                            <br> ОКПО: 41781216
                            <br> Юридический адрес: 664025, Иркутская область,
                            <br> г.Иркутск, улица Степана Разина, дом 27, офис 11</p>
                    </div>
                    <div class="col-lg-auto footer__info_first_socnet d-none d-lg-block">
                        <p>Мы в социальных сетях</p>
                        <section>
                            <a href="https://www.instagram.com/arenda.irkutsk.office/" rel=”nofollow” target="_blank">
                                <figure class="instagram"></figure>
                            </a>
                            <a href="https://vk.com/arenda_pomeshcheniy38" rel=”nofollow” target="_blank">
                                <figure class="vk"></figure>
                            </a>
                            <a href="https://www.facebook.com/%D0%9E%D0%9E%D0%9E-%D0%92%D1%8D%D0%BB%D1%82%D1%80%D0%B0%D1%81%D1%82-720149325039318/" rel=”nofollow” target="_blank">
                                <figure class="fb"></figure>
                            </a>
                        </section>
                    </div>
                </div>
                <div class="row footer__info_middle">
                    <div class="col-lg-4 offset-lg-2">
                        <p> тел/факс: <a href="tel:83952334050">(3952) 33-40-50</a>, <a href="tel:83952333378">33-33-78</a>
                            <br> E-mail: <a href="mailto:trast38@mail.ru">trast38@mail.ru</a> </p>
                    </div>
                </div>
                <div class="row footer__info_last justify-content-end">
                    <div class="col-lg-auto"> <a href="/privacy">Политика конфиденциальности</a> </div>
                </div>
            </div>
        </div>
        <div class="footer__developer"> <a href="https://litvinenko.digital/" rel=”nofollow” target="_blank">разработка сайта - LITVINENKO</a> </div>
    </footer>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/scrollweedeveryday.js"></script>
    <script type="text/javascript" src="js/parallax.js"></script>
    <script type="text/javascript" src="js/categoriesMobile.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.slick-in').slick({
                arrows: true
                , variableWidth: true
                , autoplay: true
            });
            setTimeout(500);
            $('.giphy').fadeOut(300);
            $('.block-categories__all').css('min-height', $("#podzamenu").height() + 30);
        });
    </script>
    <script>
        $(document).ready(function() {
          $('.slick-in img').magnificPopup({type:'image'});
        });
    </script>
</body>

</html>
