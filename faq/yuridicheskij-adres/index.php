<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="Разработчик Litvinenko Digital">
    <meta name="description" content="ООО «Вэлтраст» предоставляет гарантийное письмо в адрес ИФНС для регистрации юридического адреса.">
    <meta name=«title» content="Юридический адрес">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:url" content="http://arenda-pomeshcheniy38.ru/">
    <title>Юридический адрес</title>
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/png" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/faq.css">
    <!--  fonts   -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900" rel="stylesheet"></head>
    <!--  map   -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 17,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(52.283202, 104.277974), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [ { "featureType": "all", "elementType": "all", "stylers": [ { "saturation": -100 }, { "gamma": 0.5 } ] } ]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(52.283202, 104.277974),
                    map: map,
                    icon: {
                        url: "../../img/icon/map-marker.png",
                        scaledSize: new google.maps.Size(32, 64)
                    }
                });
            }
        </script><!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter47979902 = new Ya.Metrika2({
                            id:47979902,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true,
                            trackHash:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/47979902" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
<body>
    <div class="wrapper">
        <header class="header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="header__logo col-lg-auto">
                        <div class="row align-items-end m-0"> <span><img src="../../img/main/header__logo.png" alt=""></span> <span class="company">ООО «ВЭЛТРАСТ»</span> </div> <address>г.Иркутск ул. Степана Разина 27</address> </div>
                    <div class="header__menu col">
                        <ul class="nav">
                            <li class="nav-item"> <a class="nav-link" href="/"> < вернуться</a> </li>
                        </ul>
                    </div>
                    <div class="header__phone col-lg-auto"> <img src="../../img/icon/phone.jpg" alt=""> <a href="tel:83952334050">8 (3952) 33-40-50</a> </div>
                </div>
            </div>
        </header>
        <section id="block-parking">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Юридический адрес</h1>
                        <span>Могу ли я получить юридический адрес на организацию?</span>
                    </div>
                    <div class="col-lg-12">
                        <p>ООО «Вэлтраст» предоставляет гарантийное письмо в адрес ИФНС для регистрации юридического адреса.
                        <br>При посещении налоговых органов по вашей компании или ООО мы даем положительные характеристики
                        <br>о вашей организации. Помните, когда юридическим адресом ООО является квартира, то это значительно
                        <br>влияет на имидж самой организации и как ни странно доверия к ней меньше;
                        <br>Чтобы арендовать офис в Иркутске и получить юридический адрес на организацию,
                        <br>звоните нам по прямому номер: 8 (3952) 33-40-50 </p>
                    </div>
                    <div class="col-lg-12 block-parking__line">
                        <a href="/#block-categories"><button>Выбрать офисное помещение ></button></a>
                    </div>
                </div>
            </div>
        </section>
        <section id="block-map">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <span>МЫ НА КАРТЕ<br>
                        <small>г.Иркутск, улица Степана Разина, дом 27, офис 11</small></span>
                    </div>
                </div>
            </div>
            <div id="map"></div>
        </section>
    </div>
    <footer class="footer">
        <div class="footer__info">
            <div class="container">
                <div class="row footer__info_first align-items-end">
                    <div class="col-lg-2"> <img class="footer__info-img" src="../../img/main/footer__logo.png" alt=""> </div>
                    <div class="col">
                        <p>ОГРН:1123850047133
                            <br> ИНН/КПП: 380226903/38081001
                            <br> ОКПО: 41781216
                            <br> Юридический адрес: 664025, Иркутская область,
                            <br> г.Иркутск, улица Степана Разина, дом 27, офис 11</p>
                    </div>
                    <div class="col-lg-auto footer__info_first_socnet">
                        <p>Мы в социальных сетях</p>
                        <section>
                            <a href="https://www.instagram.com/arenda.irkutsk.office/" target="_blank">
                                <figure class="instagram"></figure>
                            </a>
                            <a href="https://vk.com/arenda_pomeshcheniy38" target="_blank">
                                <figure class="vk"></figure>
                            </a>
                            <a href="https://www.facebook.com/%D0%9E%D0%9E%D0%9E-%D0%92%D1%8D%D0%BB%D1%82%D1%80%D0%B0%D1%81%D1%82-720149325039318/" target="_blank">
                                <figure class="fb"></figure>
                            </a>
                        </section>
                    </div>
                </div>
                <div class="row footer__info_middle">
                    <div class="col-lg-4 offset-lg-2">
                        <p> тел/факс: <a href="tel:83952334050">(3952) 33-40-50</a>, <a href="tel:83952333378">33-33-78</a>
                            <br> E-mail: <a href="mailto:trast38@mail.ru">trast38@mail.ru</a> </p>
                    </div>
                </div>
                <div class="row footer__info_last justify-content-end">
                    <div class="col-lg-auto"> <a href="/privacy">Политика конфиденциальности</a> </div>
                </div>
            </div>
        </div>
        <div class="footer__developer"> <a href="https://litvinenko.digital/" target="_blank">разработка сайта - LITVINENKO</a> </div>
    </footer>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
</body>

</html>
